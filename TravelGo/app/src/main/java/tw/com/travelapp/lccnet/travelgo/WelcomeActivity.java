package tw.com.travelapp.lccnet.travelgo;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import tw.com.travelapp.lccnet.travelgo.login.ForgetPwFragment;
import tw.com.travelapp.lccnet.travelgo.login.LoginFragment;
import tw.com.travelapp.lccnet.travelgo.login.SignupFragment;
import tw.com.travelapp.lccnet.travelgo.login.ViewPagerAdapter;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager vp;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
    }

    private void init(){
        vp = (ViewPager) this.findViewById(R.id.viewpager);
        ArrayList<Fragment> data = new ArrayList<>();
        data.add(new LoginFragment());
        data.add(new SignupFragment());
        data.add(new ForgetPwFragment());
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),data);
        vp.setAdapter(adapter);
    }
}
