package tw.com.travelapp.lccnet.travelgo.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tw.com.travelapp.lccnet.travelgo.R;

/**
 * Created by liu on 2016/5/10.
 */
public class ForgetPwFragment extends Fragment {

    private View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_forgetpw,container,false);
        return v;
    }
}
