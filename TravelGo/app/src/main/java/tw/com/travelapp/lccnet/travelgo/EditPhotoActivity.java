package tw.com.travelapp.lccnet.travelgo;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.Calendar;

/**
 * Created by liu on 2016/5/10.
 */
public class EditPhotoActivity extends AppCompatActivity {
    private ImageButton eTB, dNB;
    private TextView moodText, timeView, locaView;
    private ImageView getPic;
    private int tY,tM,tD,tHr,tMin;
    private String lCountry,lCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_photo);



        //呼叫自動取得日期時間地點
        timeView=(TextView)findViewById(R.id.timeView);
        locaView=(TextView)findViewById(R.id.locaView);

        Calendar t=Calendar.getInstance();
        tY=t.get(Calendar.YEAR);
        tM=t.get(Calendar.MONTH);
        tD=t.get(Calendar.DAY_OF_MONTH);
        tHr=t.get(Calendar.HOUR);
        tMin=t.get(Calendar.MINUTE);


        updateDisplay();




        //編輯按鈕設定
        eTB = (ImageButton) findViewById(R.id.editTextButton);
        moodText = (TextView) findViewById(R.id.moodText);
        eTB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInPutDialog();
            }
        });



        //呼叫啟動相機
        getPic = (ImageView) findViewById(R.id.imageView);
        goCamera();


    }

    //編輯輸入匡樣式
    protected void showInPutDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(EditPhotoActivity.this);
        View promptView = layoutInflater.inflate(R.layout.alert_show, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EditPhotoActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);

        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        moodText.setText(editText.getText());
                    }

                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    //取得時間
    protected void updateDisplay(){
        timeView.setText(new StringBuilder().append(tY).append("/")
                .append(format(tM+1)).append("/")
                .append(format(tD)).append(" ")
                .append(format(tHr)).append(":")
                .append(format(tMin)).append(" ")
        );
    }
    //時間格式如果數字只有一位數加零
    protected String format(int i) {
        String s = "" + i;
        if (s.length() == 1) {
            s = "0" + s;
        }
        return s;
    }

    //啓動相機
    protected void goCamera(){
        File fi=null;
        try {
            fi=new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+"zhycheng.jpg");
        }catch (Exception e){
            e.printStackTrace();
        }
        Uri ui=Uri.fromFile(fi);

        Intent intent_camera=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent_camera.putExtra(MediaStore.Images.Media.ORIENTATION,0);
        intent_camera.putExtra(MediaStore.EXTRA_OUTPUT,ui);
        this.startActivityForResult(intent_camera,9);
    }

    //啟動相機相片後的回應動作
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK)
        {
            if (requestCode == 9)
            {
                File fii = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                        File.separator + "zhycheng.jpg");

                Intent i = new Intent("com.android.camera.action.CROP");
                i.setType("image/*");
                //i.putExtra("data", ps);
                i.setDataAndType(Uri.fromFile(fii), "image/jpeg");
                i.putExtra("crop", "true");
                i.putExtra("aspectX", 1);
                i.putExtra("aspectY", 1);
                i.putExtra("outputX", 500);
                i.putExtra("outputY", 500);
                i.putExtra("return-data", true);
                this.startActivityForResult(i, 7);

            }
            if (requestCode == 7) {
                Bitmap ps = data.getParcelableExtra("data");
                getPic.setImageBitmap(ps);
            }
        }super.onActivityResult(requestCode,resultCode,data);
    }




}
